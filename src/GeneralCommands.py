from discord.ext import commands

help_message: str = """
TJ's Discord selfbot
Commands format:
``command`` [shortcuts] - Explanation (extra notes)

Available commands:
    Functionality:
        ``test`` [t] - Test if the bot is active
        ``help`` - Shows this message

    Activity:
        ``available`` [av] - Clear changed RPC (Resets status to online and activity to 'Watching TJ being lazy''
        ``sleep`` - Sleep RPC
        ``school``[work] - School RPC
        ``homework`` [hw] - Homework RPC

    Emotes:
        ``emote`` [e] - Automated emote integration without having Discord Nitro

    List of emotes:
        `troll`, `trolla`, `crackcat`, `zzzcat`, `zzzcatconfused`, `zzzcathappy`, `5head`, `kekwdisco`, `sadangry`, `edits`, `fastsmh`, `catjamfast`, `bruh`, `sus`
"""


class GeneralCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="test", aliases=["t"])
    async def command_test(self, ctx):
        """command_test gets called when the user executes the command '>>>test' into any channel"""
        if self.bot.debug:
            print("command_test called.")

        await ctx.reply("Working.")

    @commands.command(name="reload")
    async def command_reload_cog(self, ctx, *, cog: str):
        """command_reload_cog gets called when the user executes the command '>>>reload' into any channel. It's used to reload any of the loaded cog extensions"""
        try:
            await self.bot.reload_extension(cog)
            await ctx.reply(f"Reloaded extension `{cog}` successfully.")
        except Exception as e:
            await ctx.reply(f"Failed to reload extension `{cog}`. {type(e).__name__}: {e}")

    @commands.command(name="load")
    async def command_load_cog(self, ctx, *, cog: str):
        """command_load_cog gets called when the user executes the command '>>>load' into any channel. it's used to load a newly added cog extension"""
        try:
            await self.bot.load_extension(cog)
            await ctx.reply(f"Loaded extension `{cog} successfully.")
        except Exception as e:
            await ctx.reply(f"Failed to load extension `{cog}`. {type(e).__name__}: {e}")

    @commands.command(name="help")
    async def command_help(self, ctx):
        """command_help gets called when the user executes the command '>>>help' into any channel"""
        if self.bot.debug:
            print("command_help called.")

        await ctx.reply(help_message)


async def setup(bot):
    await bot.add_cog(GeneralCommands(bot))
