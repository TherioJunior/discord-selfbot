import discord
from discord.ext import commands


class StatusCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="available", aliases=["av"])
    async def command_available(self, ctx):
        """command_available gets called when the user executes the command '>>>available' or '>>>av' into any channel"""
        if self.bot.debug:
            print("command_available called.")

        await self.bot.change_presence(
            status=discord.Status.do_not_disturb,
            activity=discord.Activity(
                type=discord.ActivityType.watching,
                name=f"{self.bot.cfg_user} being lazy"
            ),
            afk=True
        )

        await ctx.message.delete()

    @commands.command(name="sleep")
    async def command_sleep(self, ctx):
        """command_sleep gets called when the user executes the command '>>>sleep' into any channel"""
        if self.bot.debug:
            print("command_sleep called.")

        await self.bot.change_presence(
            status=discord.Status.do_not_disturb,
            activity=discord.Activity(
                type=discord.ActivityType.watching,
                name=f"{self.bot.cfg_user} sleep"
            ),
            afk=False
        )

        await ctx.message.delete()

    @commands.command(name="school")
    async def command_school(self, ctx):
        """command_school gets called when the user executes the command '>>>school' or '>>>work' into any channel"""
        if self.bot.debug:
            print("command_school called.")

        await self.bot.change_presence(
            status=discord.Status.do_not_disturb,
            activity=discord.Activity(
                type=discord.ActivityType.watching,
                name=f"{self.bot.cfg_user} suffer at school"
            ),
            afk=True
        )

        await ctx.message.delete()

    @commands.command(name="homework", aliases=["hw"])
    async def command_homework(self, ctx, *, is_afk: str = "no"):
        """command_homework gets called when the user executes the command '>>>homework' or '>>>hw' into any channel"""
        if self.bot.debug:
            print("command_homework called.")

        # is_afk logic has to be reversed, because True means you're away from your keyboard and that mobile
        # notifications can be delivered, whereas False means the exact opposite... quite counter intuitive but whatever
        if is_afk == "yes":
            is_afk = False
        else:
            is_afk = True

        await self.bot.change_presence(
            status=discord.Status.do_not_disturb,
            activity=discord.Activity(
                type=discord.ActivityType.watching,
                name=f"{self.bot.cfg_user} doing homework"
            ),
            afk=is_afk
        )

        await ctx.message.delete()

    @commands.command(name="work", aliases=["code"])
    async def command_work(self, ctx, *, is_afk: str = "no"):
        """command_work gets called when the user executes the command '>>>work' or '>>>code' into any channel"""
        if self.bot.debug:
            print("command_work called.")

        # is_afk logic has to be reversed, because True means you're away from your keyboard and that mobile
        # notifications can be delivered, whereas False means the exact opposite... quite counter intuitive but whatever
        if is_afk == "yes":
            is_afk = False
        else:
            is_afk = True

        await self.bot.change_presence(
            status=discord.Status.do_not_disturb,
            activity=discord.Activity(
                type=discord.ActivityType.watching,
                name=f"{self.bot.cfg_user} Code"
            ),
            afk=is_afk
        )

        await ctx.message.delete()

    @commands.command(name="reverse", aliases=["rev"])
    async def command_reverseengineer(self, ctx, *, executable_type: str = "Applications"):
        """command_reverseengineer gets called when the user executes the command '>>>reverse' or '>>>rev'
        into any channel"""
        if self.bot.debug:
            print("command_reverseengineer called.")

        await self.bot.change_presence(
            status=discord.Status.do_not_disturb,
            activity=discord.Activity(
                type=discord.ActivityType.watching,
                name=f"{self.bot.cfg_user} Reverse Engineer {executable_type}"
            ),
            afk=False
        )

        await ctx.message.delete()


async def setup(bot):
    await bot.add_cog(StatusCommands(bot))
