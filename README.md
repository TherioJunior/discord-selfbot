# Discord Selfbot by TherioJunior

A Discord.py Selfbot

## Description

This project is a Discord selfbot powered by [this](https://github.com/dolfies/discord.py-self) python selfbot library forked from the original discord.py library. The bot is built using Python 3.10.11 and is compatible with Python 3.8 and higher.

Its primary features include:

- Rich presence statuses for being available, school/work, sleeping & doing homework
- Automatic emote management which doesn't require discord nitro, no matter what client you're on (mobile/PC)
- Automatic URL/Embed replacement upon sending messages containing links to privacy focused frontends, to not annoy your friends with them (i.e. replace piped.kavin.rocks with youtube.com)
- Automatic improved Twitter embedding upon sending messages (i.e. twitter.com/x.com -> vxtwitter.com)

## Available commands

This section might be outdated at times, so check the code for more up to date information if you care about it.

Command prefix: >>> (Can be configured in [config.json](./src/config.json))

Username used in Custom RPCs: Discord handle by default, can be configured in [config.json](./src/config.json)

- test 
  - See if the bot is active
- help 
  - Shows a custom help message
- available 
  - Custom RPC saying 'Watching \<user> being lazy'
- sleep 
  - Custom RPC saying 'Watching \<user> sleep'
- school 
  - Custom RPC saying 'Watching \<user> suffer at school'
- homework 
  - Custom RPC saying 'Watching \<user> doing homework'
- emote 
  - Command to use any of the following emotes: `troll`, `trolla`, `crackcat`, `zzzcat`, `zzzcatconfused`, `zzzcathappy`, `5head`, `kekwdisco`, `sadangry`, `edits`, `fastsmh`, `catjamfast`, `bruh`, `sus`
- reload 
  - Reloads any of the currently loaded cogs to react to code changes on runtime without having to restart the bot
- load 
  - Loads a newly created cog on runtime without having to restart the bot

## Project Status

This project is currently under development. New features could be added at any time.

## Prerequisites

To run this project, you need Python 3.8 or higher.

## Installation

To set up this project, clone this repository with the following command:
```shell
git clone https://gitlab.com/TherioJunior/discord-selfbot
```

Set up a virtual environment
```shell
pip install virtualenv
virtualenv venv
```
Enter it

Linux:
```shell
source venv/bin/activate
```
Windows PowerShell:
```shell
.\venv\Scripts\activate
```

Install all required libraries mentioned in the `requirements.txt` file, with this command:
```python
pip install -r -U requirements.txt
```

Afterwards, create a file called `.env`, containing the following text:
```
TOKEN=YOUR_DISCORD_TOKEN_HERE
```

To obtain your discord token, you can have a look [here](https://discordpy-self.readthedocs.io/en/latest/token.html).

## Usage
To run the selfbot, execute the bot.py script as follows:

Linux:
```shell
python3 src/bot.py
```

Windows PowerShell:
```shell
python .\src\bot.py
```

## License
This project is licensed under the GPL 3.0, the terms of which can be seen [here](./LICENSE).
