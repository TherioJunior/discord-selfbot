import discord
import os
import json
from discord.ext import commands
from dotenv import load_dotenv

load_dotenv()

# Default configuration values
default_config: dict = {
    "debug": False,
    "user": "User",
    "command_prefix": ">>>"
}

# Primary and alternative configuration file paths
primary_config_file_path: str = "config.json"
alternative_config_file_path: str = "src/config.json"

# Try loading the primary configuration file
if os.path.exists(primary_config_file_path):
    with open(primary_config_file_path, "r") as config_file:
        config: dict = json.load(config_file)
# Try loading the alternative configuration file if primary is not found
elif os.path.exists(alternative_config_file_path):
    with open(alternative_config_file_path, "r") as config_file:
        config: dict = json.load(config_file)
else:
    print(f"Neither '{primary_config_file_path}' nor '{alternative_config_file_path}' found. Using default configuration.")
    config: dict = default_config

# Extracting configuration values with defaults as fallback
debug: bool = config.get("debug", default_config["debug"])
cfg_user: str = config.get("user", default_config["user"])
cmd_prefix: str = config.get("command_prefix", default_config["command_prefix"])

class MyBot(commands.Bot):
    def __init__(self, command_prefix, self_bot):
        super().__init__(
            command_prefix=command_prefix,
            self_bot=self_bot
        )

        self.remove_command("help")
        self.initial_extensions = ["EmoteCommands", "StatusCommands", "MessageCommands", "GeneralCommands"]
        self.debug = debug

        if cfg_user == "User":
            self.cfg_user = self.user
        else:
            self.cfg_user = cfg_user

    async def setup_hook(self):
        for extension in self.initial_extensions:
            await self.load_extension(extension)
            print(f"Loaded extension {extension}")

    async def on_ready(self):
        """on_ready event gets called when the bot comes online."""
        print(f"Logged in as: {self.user}")

        await self.change_presence(
            status=discord.Status.do_not_disturb,
            activity=discord.Activity(
                type=discord.ActivityType.watching,
                name=f"{self.cfg_user} being lazy"
            ),
            afk=True
        )

        if self.debug:
            print("Successfully changed presence")


client = MyBot(command_prefix=cmd_prefix, self_bot=True)
token = os.getenv("TOKEN")
client.run(token)
